﻿Alla kokemuksia Vincitin koodikoulusta. Halusimme kokeilla joitain juttuja omalla tavallamme, ja ainakaan
palautteen perusteella muutokset eivät vieneet huonompaan suuntaan, päinvastoin :)

## Mitä on koodaaminen

- Kuka on koodannut? -> yleensä suurin osa ei lainkaan
    - Voit myös kysellä lapsilta, missä kaikkialla on koodia. Viihdyttäviä vastauksia, ja lapset tykkäävät :)
- Väärin! Suurin osa on todennäköisesti "koodannut", ainakin jollain lailla ja jotain muuta kuin tietokoneita! :)
- Onko "väri" niminen leikki tuttu?
    - (Leikittäjä huutelee edestä "kaikki joilla on vihreä paita, 5 askelta eteen/taaksepäin" yms.)
    - Ihan kuin koodaaminen!
- Leikitään seuraavaksi koodaa-ope-pillimehun-luo -leikkiä.
    - Opettaja muuttuu <firman maskotti>:ksi, joka osaa vain mennä eteenpäin ja kääntyä.
    - Lapset ohjailevat open luokkatilan läpi.
- Koodaaminen on samanlaisia ohjeita, mutta open sijasta tietokoneelle!
 
- Kuka rakentanut legoilla/duploilla/palikoilla? -> yleensä kaikki
    - Kysele mitkä on hienoimpia juttuja mitä lapset ovat rakentaneet legoista.
    - Kerro myös oma lemppari, lapset tykkäävät :)
- Koodaaminen on rakentamista!
    - Leikissä ope saatiin askel kerrallaan koodattua pillimehun luo.
    - Pienimmät legopalikat ovat yksinään tylsiä, mutta kun niitä on paljon saadaan hienoja juttuja tehtyä!
    - Samalla lailla tietokoneelle koodaamalla saadaan tehtyä vaikka huimia tietokonepelejä.


Vincitin koodikouluun osallistui sen verran nuorta väkeä, että jätimme tarkemmat tarinat tietokoneen
sisuskaluista kuten prosessoreista ja muisteista kokonaan väliin. Ensikosketuksessa koodaamiseen oli
itsessään riittävästi pureskeltavaa ja hauskaa tekemistä :)
 
## Turtle Roy

Meillä oli käytössä suomenkieliset versiot peruskomennoista (myös lyhenteet). Komennot saa käyttöön helposti copy-pastella alta:
    
        let sarja = sequence
        let toista = repeat
        let eteen n = fd n
        let taakse n = sequence[lt 180, fd n, lt 180]
        let oikea n = rt n
        let vasen n = lt n
        let oikealle = rt 90
        let vasemmalle = lt 90
        let kotiin = home
        let pyyhi = clear
        let kynäylös = penup
        let kynäalas = pendown
    
        let e = eteen
        let o = oikea
        let v = vasen
        let k = kotiin
        let t = toista
        let p = pyyhi
        let ky = kynäylös
        let ka = kynäalas

    
Näiden avulla oli helppo saada lapset ymmärtämään open "koodaamisen" ja tietokoneen koodamisen yhteys, ts. koodi on vain ohjeita tietokoneelle. Suomenkieliset komennot myös vähensivät "magiaa", kun lapset kykenivät ymmärtämään komennon ja tapahtuman välisen yhteyden (sano koneelle eteen -> kilppari menee eteenpäin).

## Tapahtumaorganisointi Eventbritella
[Eventbrite](http://www.eventbrite.com) on näppärä keino sumplia ilmoittautumiset. Palvelu tarjoaa mahdollisuuden "myydä" rajattu
määrä lippuja ja kerätä muiden kiinnostuneiden yhteystiedot jonotuslistalle.

Loimme tapahtumalle sivun hyvissä ajoin ja ajastimme lipunmyynnin aukeamaan noin viikkoa ennen varsinaista koodikoulua. 
Tapahtumasta kerrottiin vain lyhyet perustiedot, osoite ja tietysti ajankohta. Varsinainen koodikoulun mainostus tehtiin muilla medioilla.

Eventbritella on kätevää lähettää kaikille osallistujille sähköpostia kerralla. Lähetimme muistutusmailin pari päivää ennen varsinaista 
tapahtumaa, viestissä oli mukana myös tarkempi kuvaus perillepääsystä karttoineen ja kuvineen. Tapahtuman jälkeen lähetimme kiitos- ja palautteenkeruuviestin.

Muutama osallistuja perui osallistumisensa sähköpostitse, jolloin jonotuslista tuli hyvään käyttöön. 
Jonosta on manuaalisesti vapautettava osallistujille lippu, mikä on hieman kömpelöä. Jonottajilta on hyvä pyytää myös puhelinnumero, koska peruutukset vaikuttavat tulevan viime tingassa ja sähköposti saattaa olla liian hidas lippujen sumplimiseen.

## Tehtävälista
Vincitin koodikoulun tehtäviä ja muistettavia asioita ylläpidettiin Trellossa. 
[Tehtävälistan pohja](https://trello.com/b/l8xDHZLd/vincit-lasten-koodikoulu-template) on kopioitavissa oman koodikoulunne järjestämiseen.